# README #

Thanks for your interest in a engineering position at [Sendit.money](http://Sendit.money). This file details a simple tech challenge to help us understand your technical ability.

Once you have completed the challenge please commit it to a private git repo, and share access with ash.nunn@sendit.money

# Brief

For this challenge we are going to create a simple prototype app to display a list of **Transactions.** in order to achieve this you will also be building a Data-model, List view and Supporting api endpoints.

You have been provided with boilerplate code for both react native for the frontend and Lumen (php) for the backend, you are free to continue with these setups or roll your own.

The only requirements are that react native (typescript) and php are used to provide your solution.

### Transactions

The transaction Data-model(s) that you create must have at least the following properties and relationships, you are of course free to add any further properties you deem required or useful.

```json
transaction: {
	id: uuid;
	user: uuid (user relation);
	amount: float;
	sendingAccount: uuid (account relation);
	receivingAccount: uuid (account relation);
	startedAt: datetime;
	completedAt: datetime;
}
```

The Lumen boilerplate contains functionality for linking to a MySql Datastore and defining migrations and seeds to populate your data. Again you are free to use this setup or roll your own using whatever connection framework or Database technology you like, as long as you provide the functionality to migrate and seed data as part of testing your solution.

# Deliverables

Successful completion of the challenge requires the delivery of the below features

- React Native app that will run in both iOs and android with the following:
    - Displayed list of transactions ordered with the most recent transaction first
    - Displayed list of transactions that can be filtered by user
- API process that can be run locally that provides the following:
    - Data-model definitions for the **Transaction** data types
    - Data-model definitions tor any supporting data types
    - Seed file to create a range of test data for all present data types
    - GET Endpoints to retrieve **Transactions** data

The challenge is intentionally let open. While the above deliverables are required you are free to extend or add anything you wish including examples of testing or user authentication. However please try and keep to a maximum of around 2 or hours of development.



# How do I get set up? ###

## Prerequisites

you will need the following to start working on the challenge

* node >= 14
* PHP >= 8.0
* composer >= 2.2.7


## Steps

to start the app project you undertake the following step once the repo has been cloned

* cd app/
* npm install
* expo start

to start the app project you undertake the following step once the repo has been cloned

* cd api/
* composer install
* php -S localhost:8000 -t public


# Who do I talk to? ###

* Any questions can be directed to ash.nunn@sendit.money.
